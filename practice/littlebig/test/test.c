#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

bool isBigendian(void) {
	if ( htonl(47) == 47 ) {
		// Big endian.
		printf("this is bigendian\n");
		return true;
	} else {
		// Little endian.
		printf("this is littleendian\n");
		return false;
	}
}

int testEndian(void) {
    char appOut[16] = {0};
    int outSize;

    FILE* handler = popen("./littlebig", "r");
    if (handler == NULL) return 1;

    outSize = fread(appOut, sizeof(char), sizeof(appOut), handler);
    pclose(handler);

	if(isBigendian()) {
		return strncmp(appOut, "big", outSize < 3 ? outSize : 3);
	} else {
		return strncmp(appOut, "little", outSize < 5 ? outSize : 5);			
	}
}

int main(int argc, char *argv[])
{
  return testEndian();
}

